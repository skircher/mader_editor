import { UIPanel, UIRow, UIHorizontalRule } from './libs/ui.js';

import { AddObjectCommand } from './commands/AddObjectCommand.js';
import { RemoveObjectCommand } from './commands/RemoveObjectCommand.js';

function MenubarEdit( editor ) {

	var strings = editor.strings;

	var container = new UIPanel();
	container.setClass( 'menu' );

	var title = new UIPanel();
	title.setClass( 'title' );
	title.setTextContent( "Edition" );
	container.add( title );

	var options = new UIPanel();
	options.setClass( 'options' );
	container.add( options );

	// Undo

	var undo = new UIRow();
	undo.setClass( 'option' );
	undo.setTextContent( "Annuler" );
	undo.onClick( function () {

		editor.undo();

	} );
	options.add( undo );

	// Redo

	var redo = new UIRow();
	redo.setClass( 'option' );
	redo.setTextContent( "Refaire" );
	redo.onClick( function () {

		editor.redo();

	} );
	options.add( redo );

	return container;

}

export { MenubarEdit };
