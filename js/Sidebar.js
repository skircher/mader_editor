import { UITabbedPanel, UISpan } from "./libs/ui.js";

import { SidebarScene } from "./Sidebar.Scene.js";
import { SidebarProperties } from "./Sidebar.Properties.js";
import { SidebarScript } from "./Sidebar.Script.js";
import { SidebarAnimation } from "./Sidebar.Animation.js";
import { SidebarProject } from "./Sidebar.Project.js";
import { SidebarHistory } from "./Sidebar.History.js";
import { SidebarSettings } from "./Sidebar.Settings.js";

function Sidebar(editor) {
  var strings = editor.strings;

  var container = new UITabbedPanel();
  container.setId("sidebar");

  var scene = new UISpan().add(
    new SidebarScene(editor),
    new SidebarProperties(editor),
    new SidebarAnimation(editor),
    new SidebarScript(editor),
    new SidebarProject(editor)
  );

  var project = new SidebarProject(editor);

  var settings = new UISpan().add(
    new SidebarSettings(editor),
    new SidebarHistory(editor)
  );

  container.addTab("scene", strings.getKey("sidebar/scene"), scene);
  container.select("scene");

  return container;
}

export { Sidebar };
