import * as THREE from "../../build/three.module.js";

import {
  UIPanel,
  UIRow,
  UIHorizontalRule,
  UISelect,
  UIText,
} from "./libs/ui.js";

import { AddObjectCommand } from "./commands/AddObjectCommand.js";

function get(url, token) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", url, false); // false for synchronous request
  xmlHttp.setRequestHeader("Authorization", token);
  xmlHttp.send(null);
  return JSON.parse(xmlHttp.responseText);
}

const API_URL_BACKEND = "http://localhost:8282";

function MenubarAdd(editor, token) {
  let listModule = get(API_URL_BACKEND + "/api/v1/moduleclient", token);
  let listFamilleModule = get(API_URL_BACKEND + "/api/v1/famillemodule", token);

  var strings = editor.strings;

  var container = new UIPanel();
  container.setClass("menu");

  var title = new UIPanel();
  title.setClass("title");
  title.setTextContent("Ajouter Module");
  container.add(title);

  var options = new UIPanel();
  options.setClass("options");
  container.add(options);

  // Mes familles et composants
  listFamilleModule.forEach((element) => {
    const famille = element.nom;
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    var familleNom = new UIText(famille);
    options.add(familleNom);
    var composantSelect = new UISelect();

    const listModuleFiltre = listModule.filter(
      (module) => module.familleModule.nom === famille
    );

    const listModuleFiltreByGamme = listModuleFiltre.filter(
      (module) =>
        module.gammes?.find(
          (gammeFilter) => gammeFilter.nom === urlParams.get("gamme")
        ) !== undefined
    );

    const mesComposants = {};
    listModuleFiltreByGamme.forEach((element) => {
      mesComposants[element.nom] = element.nom;
    });
    composantSelect.setOptions(mesComposants).setWidth("130px");
    composantSelect.onChange(function () {
      const loader = new THREE.FileLoader();
      loader.load(
        // resource URL
        "../modelisation/models/threejs/JSON/" +
          composantSelect.getValue() +
          ".json",
        // onLoad callback
        function (data) {
          var loader = new THREE.ObjectLoader();
          loader.parse(JSON.parse(data), function (result) {
            if (result.isScene) {
              editor.execute(new SetSceneCommand(editor, result));
            } else {
              editor.execute(new AddObjectCommand(editor, result));
            }
          });
        },
        // onProgress callback
        function (xhr) {
          console.log((xhr.loaded / xhr.total) * 100 + "% loaded");
        },

        // onError callback
        function (err) {
          console.error("An error happened");
        }
      );
      composantSelect.setValue(null);
    });
    options.add(composantSelect);
    options.add(new UIHorizontalRule());
  });

  //

  return container;
}

export { MenubarAdd };
