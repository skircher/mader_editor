import * as THREE from "../../build/three.module.js";

import { ColladaExporter } from "../../examples/jsm/exporters/ColladaExporter.js";
import { DRACOExporter } from "../../examples/jsm/exporters/DRACOExporter.js";
import { GLTFExporter } from "../../examples/jsm/exporters/GLTFExporter.js";
import { OBJExporter } from "../../examples/jsm/exporters/OBJExporter.js";
import { PLYExporter } from "../../examples/jsm/exporters/PLYExporter.js";
import { STLExporter } from "../../examples/jsm/exporters/STLExporter.js";

import { JSZip } from "../../examples/jsm/libs/jszip.module.min.js";

import { UIPanel, UIRow, UIHorizontalRule } from "./libs/ui.js";

function MenubarFile(editor, token) {
  function parseNumber(key, value) {
    var precision = config.getKey("exportPrecision");

    return typeof value === "number"
      ? parseFloat(value.toFixed(precision))
      : value;
  }

  function listGroupScene(scene, listModule) {
    for (const element of scene) {
      if (element.name !== "Scene") {
        let modulePush = element.name.replace(".glb", "");
        listModule.push(
          modulePush.charAt(0).toUpperCase() + modulePush.slice(1)
        );
      } else {
        listGroupScene(element.children, listModule);
      }
    }
  }

  //POST & GET REQUEST//

  function get(url) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return JSON.parse(xmlHttp.responseText);
  }

  function post(url, params, token) {
    var json = JSON.stringify(params);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Authorization", token);
    xhr.send(json);
  }
  function uploadSingleFile(url, file, produit, token) {
    var formData = new FormData();
    const myFile = new File(
      [new Blob([file], { type: "application/json" })],
      produit + ".json"
    );
    formData.append("file", myFile);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.setRequestHeader("Authorization", token);
    xhr.send(formData);
  }

  function uploadPicture(url, file, produit, token, typeVue) {
    var formData = new FormData();
    const myFile = dataURLtoFile(file, produit + typeVue);
    formData.append("file", myFile);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.setRequestHeader("Authorization", token);
    xhr.send(formData);
  }
  function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  //

  var config = editor.config;
  var strings = editor.strings;
  const API_URL_BACKEND = "http://localhost:8282";

  var container = new UIPanel();
  container.setClass("menu");

  var title = new UIPanel();
  title.setClass("title");
  title.setTextContent("Fichier");
  container.add(title);

  var options = new UIPanel();
  options.setClass("options");
  container.add(options);

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);

  // const token = getToken(urlParams.get('keyToken'))

  // console.log(token)

  // New

  var option = new UIRow();
  option.setClass("option");
  option.setTextContent("Reset Maison");
  option.onClick(function () {
    if (confirm("Souhaitez-vous vraiment supprimer la maison en cours ?")) {
      editor.clear();
    }
  });
  options.add(option);

  //

  options.add(new UIHorizontalRule());

  // Sauvegarde du produit
  const url = API_URL_BACKEND + "/api/v1/produit/modulecomposeproduit";
  const url2 = API_URL_BACKEND + "/uploadFile";
  const produitId = urlParams.get("produitId");

  var option = new UIRow();
  option.setClass("option");
  option.setTextContent("Sauvegarde Produit");
  option.onClick(function () {
    let listModule = [];
    listGroupScene(editor.scene.children, listModule);
    var renderer = new THREE.WebGLRenderer({
      antialias: true,
    });
    renderer.setSize(900, 500);
    renderer.setClearColor("#d9dddc");

    renderer.render(editor.scene, editor.camera);
    var strMime = "image/jpeg";
    var imgData = renderer.domElement.toDataURL(strMime);
    uploadPicture(url2, imgData, produitId, token, "_vue_trigo.jpg");

    //Vue en Z
    let newCamera = new THREE.PerspectiveCamera(45, 35 / 25, 1, 2000);
    newCamera.position.set(0, 2.5, 30);
    let plane = [new THREE.Plane(new THREE.Vector3(0, 0, -1), 0)];
    renderer.clippingPlanes = plane;
    renderer.render(editor.scene, newCamera);
    imgData = renderer.domElement.toDataURL(strMime);
    uploadPicture(url2, imgData, produitId, token, "_vue_axe_z.jpg");

    //Vue en x
    const vectorY = new THREE.Vector3(0, 1, 0);
    newCamera.position.set(15, 2.5, 0);
    newCamera.rotateOnAxis(vectorY, 1.5708);
    plane = [new THREE.Plane(new THREE.Vector3(-1, 0, 0), 0)];
    renderer.clippingPlanes = plane;
    renderer.render(editor.scene, newCamera);
    imgData = renderer.domElement.toDataURL(strMime);
    uploadPicture(url2, imgData, produitId, token, "_vue_axe_x.jpg");

    //Vue en y
    const vectorZ = new THREE.Vector3(1, 0, 0);
    newCamera.position.set(0, 35, 0);
    newCamera.rotateOnAxis(vectorZ, -1.5708);
    plane = [new THREE.Plane(new THREE.Vector3(0, -1, 0), 4)];
    renderer.clippingPlanes = plane;
    renderer.render(editor.scene, newCamera);
    imgData = renderer.domElement.toDataURL(strMime);
    uploadPicture(url2, imgData, produitId, token, "_vue_axe_y.jpg");

    var output = editor.scene.toJSON();
    output = JSON.stringify(output);

    uploadSingleFile(url2, output, produitId, token);

    const param = {
      nom: listModule,
      produitId: parseInt(produitId),
    };
    post(url, param, token);
  });
  options.add(option);

  //

  options.add(new UIHorizontalRule());

  var link = document.createElement("a");
  function save(blob, filename) {
    link.href = URL.createObjectURL(blob);
    link.download = filename || "data.json";
    link.dispatchEvent(new MouseEvent("click"));

    // URL.revokeObjectURL( url ); breaks Firefox...
  }

  function saveArrayBuffer(buffer, filename) {
    save(new Blob([buffer], { type: "application/octet-stream" }), filename);
  }

  function saveString(text, filename) {
    save(new Blob([text], { type: "text/plain" }), filename);
  }

  function getAnimations(scene) {
    var animations = [];

    scene.traverse(function (object) {
      var objectAnimations = editor.animations[object.uuid];

      if (objectAnimations !== undefined) animations.push(...objectAnimations);
    });

    return animations;
  }

  return container;
}

export { MenubarFile };
