import * as THREE from "../../build/three.module.js";

import {
  UIPanel,
  UIRow,
  UIInput,
  UICheckbox,
  UIText,
  UIListbox,
  UISpan,
  UIButton,
  UISelect,
  UINumber,
} from "./libs/ui.js";
import { UIBoolean } from "./libs/ui.three.js";

import { SetMaterialCommand } from "./commands/SetMaterialCommand.js";

function SidebarProject(editor) {
  var config = editor.config;
  var signals = editor.signals;
  var strings = editor.strings;

  var currentRenderer = null;

  var container = new UISpan();

  var projectsettings = new UIPanel();
  projectsettings.setBorderTop("0");
  projectsettings.setPaddingTop("20px");

  container.add(projectsettings);

  // Plan de coupe
  var cutPlan = new UIRow();
  var cutPlanButton = new UIButton("Coupe").onClick(function () {
    const plane = [new THREE.Plane(new THREE.Vector3(0, -1, 0), 4)];

    currentRenderer.clippingPlanes = plane;
    signals.rendererChanged.dispatch(currentRenderer);
  });
  cutPlan.add(cutPlanButton);
  projectsettings.add(cutPlan);

  var notCutPlan = new UIRow();
  var notCutPlanButton = new UIButton("Plein").onClick(function () {
    currentRenderer.clippingPlanes = [];
    signals.rendererChanged.dispatch(currentRenderer);
  });
  notCutPlan.add(notCutPlanButton);
  projectsettings.add(notCutPlan);

  //

  function createRenderer() {
    currentRenderer = new THREE.WebGLRenderer();
    currentRenderer.outputEncoding = THREE.sRGBEncoding;

    signals.rendererChanged.dispatch(currentRenderer);
  }

  createRenderer();

  // Materials

  var materials = new UIPanel();

  var headerRow = new UIRow();
  headerRow.add(
    new UIText(strings.getKey("sidebar/project/materials").toUpperCase())
  );

  var listbox = new UIListbox();

  var buttonsRow = new UIRow();
  buttonsRow.setPadding("10px 0px");
  materials.add(buttonsRow);

  /*
	var addButton = new UI.Button().setLabel( 'Add' ).setMarginRight( '5px' );
	addButton.onClick( function () {

		editor.addMaterial( new THREE.MeshStandardMaterial() );

	} );
	buttonsRow.add( addButton );
	*/

  var assignMaterial = new UIButton()
    .setLabel(strings.getKey("sidebar/project/Assign"))
    .setMargin("0px 5px");
  assignMaterial.onClick(function () {
    var selectedObject = editor.selected;

    if (selectedObject !== null) {
      var oldMaterial = selectedObject.material;

      // only assing materials to objects with a material property (e.g. avoid assigning material to THREE.Group)

      if (oldMaterial !== undefined) {
        var material = editor.getMaterialById(parseInt(listbox.getValue()));

        if (material !== undefined) {
          editor.removeMaterial(oldMaterial);
          editor.execute(
            new SetMaterialCommand(editor, selectedObject, material)
          );
          editor.addMaterial(material);
        }
      }
    }
  });

  container.add(materials);

  // signals

  signals.editorCleared.add(function () {
    currentRenderer.physicallyCorrectLights = false;
    currentRenderer.shadowMap.enabled = true;
    currentRenderer.shadowMap.type = 1;
    currentRenderer.toneMapping = 0;
    currentRenderer.toneMappingExposure = 1;

    refreshRendererUI();

    signals.rendererUpdated.dispatch();
  });

  function refreshRendererUI() {
    physicallyCorrectLightsBoolean.setValue(
      currentRenderer.physicallyCorrectLights
    );
    shadowsBoolean.setValue(currentRenderer.shadowMap.enabled);
    shadowTypeSelect.setValue(currentRenderer.shadowMap.type);
    toneMappingSelect.setValue(currentRenderer.toneMapping);
    toneMappingExposure.setValue(currentRenderer.toneMappingExposure);
    toneMappingExposure.setDisplay(
      currentRenderer.toneMapping === 0 ? "none" : ""
    );
  }

  signals.rendererUpdated.add(function () {
    config.setKey(
      "project/renderer/antialias",
      antialiasBoolean.getValue(),
      "project/renderer/physicallyCorrectLights",
      physicallyCorrectLightsBoolean.getValue(),
      "project/renderer/shadows",
      shadowsBoolean.getValue(),
      "project/renderer/shadowType",
      parseFloat(shadowTypeSelect.getValue()),
      "project/renderer/toneMapping",
      parseFloat(toneMappingSelect.getValue()),
      "project/renderer/toneMappingExposure",
      toneMappingExposure.getValue()
    );
  });

  signals.objectSelected.add(function (object) {
    if (object !== null) {
      var index = Object.values(editor.materials).indexOf(object.material);
      listbox.selectIndex(index);
    }
  });

  signals.materialAdded.add(refreshMaterialBrowserUI);
  signals.materialChanged.add(refreshMaterialBrowserUI);
  signals.materialRemoved.add(refreshMaterialBrowserUI);

  function refreshMaterialBrowserUI() {
    listbox.setItems(Object.values(editor.materials));
  }

  return container;
}

export { SidebarProject };
